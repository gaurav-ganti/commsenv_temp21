# Paris Agreement Consistent Categorisation of Mitigation Scenarios

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)


This repository contains the scripts that accompany the paper
> Schleussner, CF., Ganti, G., Rogelj, J., Gidden, M.J. An emission pathway classification reflecting the Paris Agreement climate objectives. Commun Earth Environ 3, 135 (2022). https://doi.org/10.1038/s43247-022-00467-w

The IPCC AR6 WG III scenario data that is used in this paper can be obtained from [here](https://data.ene.iiasa.ac.at/ar6/). We use a ```.env``` file to help you locate the data in case you already have it downloaded for another project. Please follow the following instructions:
1. Rename the ```.env.sample``` file to ```.env```
2. Copy the path to the AR6 data and metadata (v1.0)

## Citations
If you find the work in this repository helpful, please cite our paper as well as the following:

Chapter 3 of the WG III contribution to the IPCC's 6th Assessment Report
> Riahi, K. et al. Mitigation pathways compatible with long-term goals. in IPCC, 2022: Climate Change 2022: Mitigation of Climate Change. Contribution of Working Group III to the Sixth Assessment Report of the Intergovernmental Panel on Climate Change (eds. Shukla, P. R. et al.) (Cambridge University Press, 2022). doi:10.1017/9781009157926.005.

The AR6 scenario database hosted by IIASA
> Byers, E. et al. AR6 Scenarios Database. (2022) doi:10.5281/zenodo.5886912.

## Installation 
```
$ conda env create --file environment.yml
$ conda activate scenario_categorisation
```
