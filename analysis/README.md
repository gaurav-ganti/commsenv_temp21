# Files
* `1.1_ar6_data_preparation.ipynb`: In this notebook we downselect the AR6 global data.
* `1.2_categorise_ar6_data.ipynb`: In this notebook we create the new categories presented in this paper as an additional metadata column.
* `2.1_fig1_table1.ipynb`- `2.4_fig5.ipynb`: The numbers following fig and table in the filenames indicate the figure in the main text that is plotted in these notebooks.